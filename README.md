# Proportion of women in the privet sector

the data set represents the distribution of the female employees and the total employees in the private sector (excluding the Free zone) by the following attributes: 
- (1) gender
- (2) age class
- (3) emirate
- (4) skill level as per labour law in UAE 
- (5) the occupation at the level of major group of the International Standard Classification of Occupations ISCO-08
- (6) the economic activity at the level of section as per the International Standard of Industries Classification version 3.1 (ISIC 3.1). 2011-2019"

## Dataset reference
https://admin.bayanat.ae/Home/DatasetInfo?dID=CfV_VsFm8y0kX0e3Vpk2w98zki9h5gRx0wcMNPNqKZk&langKey=en

# Imports


```python
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
```

# Read Data


```python
df = pd.read_excel('data/Proportionofwomenintheprivetsector2011-2019.xlsx')
```

# Data Info


```python
df.info()
```

    <class 'pandas.core.frame.DataFrame'>
    RangeIndex: 108521 entries, 0 to 108520
    Data columns (total 17 columns):
     #   Column         Non-Null Count   Dtype  
    ---  ------         --------------   -----  
     0   Year           108521 non-null  int64  
     1   Age_Class_En   108521 non-null  object 
     2   Age_Class_Ar   108521 non-null  object 
     3   Emirate_EN     108521 non-null  object 
     4   Emirate_AR     108521 non-null  object 
     5   City_EN        108521 non-null  object 
     6   City_AR        108521 non-null  object 
     7   LONGITUDE      108521 non-null  float64
     8   LATITUDE       108521 non-null  float64
     9   Activity_EN    108515 non-null  object 
     10  Activity_AR    108515 non-null  object 
     11  Occupation_EN  108521 non-null  object 
     12  Occupation_AR  108521 non-null  object 
     13  Male           107438 non-null  float64
     14  Femal          84089 non-null   float64
     15  Total          108521 non-null  int64  
     16  POW_PS_Count   108521 non-null  float64
    dtypes: float64(5), int64(2), object(10)
    memory usage: 14.1+ MB
    


```python
df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Year</th>
      <th>Age_Class_En</th>
      <th>Age_Class_Ar</th>
      <th>Emirate_EN</th>
      <th>Emirate_AR</th>
      <th>City_EN</th>
      <th>City_AR</th>
      <th>LONGITUDE</th>
      <th>LATITUDE</th>
      <th>Activity_EN</th>
      <th>Activity_AR</th>
      <th>Occupation_EN</th>
      <th>Occupation_AR</th>
      <th>Male</th>
      <th>Femal</th>
      <th>Total</th>
      <th>POW_PS_Count</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>201112</td>
      <td>25-29</td>
      <td>25-29</td>
      <td>Sharjah</td>
      <td>الشارقة</td>
      <td>Sharjah</td>
      <td>الشارقة</td>
      <td>55.513641</td>
      <td>25.322327</td>
      <td>Manufacturing</td>
      <td>الصناعة التحويلية</td>
      <td>7-Craft and related trades workers</td>
      <td>7-الحرفيون في البناء والمهن الإستخراجية والحرف...</td>
      <td>10337.0</td>
      <td>43.0</td>
      <td>10380</td>
      <td>0.004143</td>
    </tr>
    <tr>
      <th>1</th>
      <td>201112</td>
      <td>40-44</td>
      <td>40-44</td>
      <td>Dubai</td>
      <td>دبى</td>
      <td>Dubai</td>
      <td>دبى</td>
      <td>55.296249</td>
      <td>25.276987</td>
      <td>Manufacturing</td>
      <td>الصناعة التحويلية</td>
      <td>3-Technicians and associate professionals</td>
      <td>3-  الفنيون في المواضيع العلمية والفنية والإنس...</td>
      <td>1012.0</td>
      <td>39.0</td>
      <td>1051</td>
      <td>0.037108</td>
    </tr>
    <tr>
      <th>2</th>
      <td>201112</td>
      <td>25-29</td>
      <td>25-29</td>
      <td>Abu Dhabi</td>
      <td>أبو ظبي</td>
      <td>Abu Dhabi</td>
      <td>أبو ظبي</td>
      <td>54.366669</td>
      <td>24.466667</td>
      <td>Financial intermediation</td>
      <td>الوساطة المالية</td>
      <td>4-Clerical support workers</td>
      <td>4- المهــــــــن الكتابيــــة</td>
      <td>522.0</td>
      <td>346.0</td>
      <td>868</td>
      <td>0.398618</td>
    </tr>
    <tr>
      <th>3</th>
      <td>201112</td>
      <td>20-24</td>
      <td>20-24</td>
      <td>Abu Dhabi</td>
      <td>أبو ظبي</td>
      <td>Abu Dhabi</td>
      <td>أبو ظبي</td>
      <td>54.366669</td>
      <td>24.466667</td>
      <td>Business activities</td>
      <td>خدمات الاعمال</td>
      <td>3-Technicians and associate professionals</td>
      <td>3-  الفنيون في المواضيع العلمية والفنية والإنس...</td>
      <td>324.0</td>
      <td>49.0</td>
      <td>373</td>
      <td>0.131367</td>
    </tr>
    <tr>
      <th>4</th>
      <td>201112</td>
      <td>55-59</td>
      <td>55-59</td>
      <td>Ras Al Khaima</td>
      <td>رأس الخيمة</td>
      <td>Ras El-Khaema</td>
      <td>رأس الخيمة</td>
      <td>55.976200</td>
      <td>25.800694</td>
      <td>Manufacturing</td>
      <td>الصناعة التحويلية</td>
      <td>8-Plant and machine operators, and assemblers</td>
      <td>8-  مشغلو الآلات والمعدات ومجمعوها</td>
      <td>57.0</td>
      <td>0.0</td>
      <td>57</td>
      <td>0.000000</td>
    </tr>
  </tbody>
</table>
</div>




```python
df.sample(3)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Year</th>
      <th>Age_Class_En</th>
      <th>Age_Class_Ar</th>
      <th>Emirate_EN</th>
      <th>Emirate_AR</th>
      <th>City_EN</th>
      <th>City_AR</th>
      <th>LONGITUDE</th>
      <th>LATITUDE</th>
      <th>Activity_EN</th>
      <th>Activity_AR</th>
      <th>Occupation_EN</th>
      <th>Occupation_AR</th>
      <th>Male</th>
      <th>Femal</th>
      <th>Total</th>
      <th>POW_PS_Count</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>26829</th>
      <td>201312</td>
      <td>30-34</td>
      <td>30-34</td>
      <td>Ajman</td>
      <td>عجمان</td>
      <td>Ajman</td>
      <td>عجمان</td>
      <td>55.43504</td>
      <td>25.41111</td>
      <td>Business activities</td>
      <td>خدمات الاعمال</td>
      <td>3-Technicians and associate professionals</td>
      <td>3-  الفنيون في المواضيع العلمية والفنية والإنس...</td>
      <td>111.0</td>
      <td>18.0</td>
      <td>129</td>
      <td>0.139535</td>
    </tr>
    <tr>
      <th>101710</th>
      <td>201912</td>
      <td>40-44</td>
      <td>40-44</td>
      <td>Ajman</td>
      <td>عجمان</td>
      <td>Ajman</td>
      <td>عجمان</td>
      <td>55.43504</td>
      <td>25.41111</td>
      <td>Business activities</td>
      <td>خدمات الاعمال</td>
      <td>7-Craft and related trades workers</td>
      <td>7-الحرفيون في البناء والمهن الإستخراجية والحرف...</td>
      <td>146.0</td>
      <td>3.0</td>
      <td>149</td>
      <td>0.020134</td>
    </tr>
    <tr>
      <th>91752</th>
      <td>201812</td>
      <td>50-54</td>
      <td>50-54</td>
      <td>Fujairah</td>
      <td>الفجيرة</td>
      <td>Fujiarah</td>
      <td>الفجيرة</td>
      <td>56.26176</td>
      <td>25.59246</td>
      <td>Business activities</td>
      <td>خدمات الاعمال</td>
      <td>2-Professionals</td>
      <td>2-  الإختصاصيون في المواضيع العلمية والفنية وا...</td>
      <td>30.0</td>
      <td>NaN</td>
      <td>30</td>
      <td>0.000000</td>
    </tr>
  </tbody>
</table>
</div>



# Data Cleaning


```python
df2 = df
```

### Drop Useless Columns


```python
df2.columns
```




    Index(['Year', 'Age_Class_En', 'Age_Class_Ar', 'Emirate_EN', 'Emirate_AR',
           'City_EN', 'City_AR', 'LONGITUDE', 'LATITUDE', 'Activity_EN',
           'Activity_AR', 'Occupation_EN', 'Occupation_AR', 'Male', 'Femal',
           'Total', 'POW_PS_Count'],
          dtype='object')




```python
df2 = df2.drop('Age_Class_Ar', axis = 1)
df2 = df2.drop('Emirate_AR', axis = 1)
df2 = df2.drop('City_AR', axis = 1)
df2 = df2.drop('Activity_AR', axis = 1)
df2 = df2.drop('Occupation_AR', axis = 1)
```

### Handle Missing Values


```python
df2.isnull().sum()
```




    Year                 0
    Age_Class_En         0
    Emirate_EN           0
    City_EN              0
    LONGITUDE            0
    LATITUDE             0
    Activity_EN          6
    Occupation_EN        0
    Male              1083
    Femal            24432
    Total                0
    POW_PS_Count         0
    dtype: int64




```python
df2.isnull().sum().max()
```




    24432




```python
df2 = df2.dropna()
```


```python
df2.isnull().sum()
```




    Year             0
    Age_Class_En     0
    Emirate_EN       0
    City_EN          0
    LONGITUDE        0
    LATITUDE         0
    Activity_EN      0
    Occupation_EN    0
    Male             0
    Femal            0
    Total            0
    POW_PS_Count     0
    dtype: int64



### Duplications


```python
df2.duplicated().sum()
```




    0




```python
df2[df2.duplicated()]
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Year</th>
      <th>Age_Class_En</th>
      <th>Emirate_EN</th>
      <th>City_EN</th>
      <th>LONGITUDE</th>
      <th>LATITUDE</th>
      <th>Activity_EN</th>
      <th>Occupation_EN</th>
      <th>Male</th>
      <th>Femal</th>
      <th>Total</th>
      <th>POW_PS_Count</th>
    </tr>
  </thead>
  <tbody>
  </tbody>
</table>
</div>



### Rename Columns


```python
df2.columns
```




    Index(['Year', 'Age_Class_En', 'Emirate_EN', 'City_EN', 'LONGITUDE',
           'LATITUDE', 'Activity_EN', 'Occupation_EN', 'Male', 'Femal', 'Total',
           'POW_PS_Count'],
          dtype='object')




```python
df2.columns = ['Year', 'Age_Class', 'Emirate', 'City', 'LONGITUDE',
       'LATITUDE', 'Activity', 'Occupation', 'Male', 'Female', 'Total',
       'POW_PS_Count']
```


```python
df2['Occupation'].unique().tolist()
```




    ['7-Craft and related trades workers',
     '3-Technicians and associate professionals',
     '4-Clerical support workers',
     '8-Plant and machine operators, and assemblers',
     '9-Elementary occupations',
     '5-Service and sales workers',
     '1-Managers',
     '6-Skilled agricultural, forestry and fishery workers',
     '2-Professionals',
     'X-NA']




```python
df2['Age_Class'].unique().tolist()
```




    ['25-29',
     '40-44',
     '20-24',
     '55-59',
     '30-34',
     '45-49',
     '35-39',
     '50-54',
     '65-69',
     '70+',
     '60-64',
     '16-19',
     '<16']




```python
df2['City'].unique().tolist()
```




    ['Sharjah',
     'Dubai',
     'Abu Dhabi',
     'Ras El-Khaema',
     'Madina Zayed',
     'Fujiarah',
     'Kalbaa',
     'Al-Ain',
     'Om-El-Quain',
     'Ajman',
     'Zones-Corp Abu Dhabi',
     'Khorfkan',
     'Delma',
     'Zones-Corp Al-Ain']




```python
df2['POW_PS_Count'].describe()
```




    count    83000.000000
    mean         0.144126
    std          0.222340
    min          0.000000
    25%          0.000000
    50%          0.033333
    75%          0.200000
    max          1.000000
    Name: POW_PS_Count, dtype: float64




```python
df2['Emirate'].unique().tolist()
```




    ['Sharjah',
     'Dubai',
     'Abu Dhabi',
     'Ras Al Khaima',
     'Fujairah',
     'Um Al Qaiwain',
     'Ajman']




```python
df2.sample(3)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Year</th>
      <th>Age_Class</th>
      <th>Emirate</th>
      <th>City</th>
      <th>LONGITUDE</th>
      <th>LATITUDE</th>
      <th>Activity</th>
      <th>Occupation</th>
      <th>Male</th>
      <th>Female</th>
      <th>Total</th>
      <th>POW_PS_Count</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>43173</th>
      <td>201412</td>
      <td>70+</td>
      <td>Sharjah</td>
      <td>Khorfkan</td>
      <td>55.513641</td>
      <td>25.322327</td>
      <td>Manufacturing</td>
      <td>1-Managers</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>1</td>
      <td>0.000000</td>
    </tr>
    <tr>
      <th>61622</th>
      <td>201612</td>
      <td>25-29</td>
      <td>Um Al Qaiwain</td>
      <td>Om-El-Quain</td>
      <td>55.713391</td>
      <td>25.520482</td>
      <td>Business activities</td>
      <td>5-Service and sales workers</td>
      <td>11.0</td>
      <td>1.0</td>
      <td>12</td>
      <td>0.083333</td>
    </tr>
    <tr>
      <th>57981</th>
      <td>201512</td>
      <td>65-69</td>
      <td>Abu Dhabi</td>
      <td>Madina Zayed</td>
      <td>54.366669</td>
      <td>24.466667</td>
      <td>Transport, storage and communications</td>
      <td>2-Professionals</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>1</td>
      <td>0.000000</td>
    </tr>
  </tbody>
</table>
</div>




```python
df2['Total'].describe()
```




    count    83000.000000
    mean       480.442843
    std       2599.189507
    min          1.000000
    25%          5.000000
    50%         27.000000
    75%        155.000000
    max      90032.000000
    Name: Total, dtype: float64




```python
df2['Age_Class'].unique().tolist()
```




    ['25-29',
     '40-44',
     '20-24',
     '55-59',
     '30-34',
     '45-49',
     '35-39',
     '50-54',
     '65-69',
     '70+',
     '60-64',
     '16-19',
     '<16']



### Save changes after cleaning data


```python
df = df2
```

# Statistics

### 1. What is the average count of Female workers in the UAE's private sectors?


```python
df['Female'].mean()
```




    44.0555421686747



### 2. What is the average count of Female workers in the Financial intermediation sectors in the UAE?


```python
df[ df['Activity'] == 'Financial intermediation']["Female"].mean()
```




    35.00450349020491



### 3. What is the count of Sectors that contains more than 2000 Female worker and located in Dubai Emirate?


```python
len(df[(df['Female'] > 2000) & (df['Emirate'] == 'Dubai')])
```




    227



### 4. What is the total count of Female workers for each Age class?


```python
df.groupby("Age_Class")['Female'].sum()
```




    Age_Class
    16-19      7309.0
    20-24    286427.0
    25-29    873663.0
    30-34    902152.0
    35-39    644552.0
    40-44    390545.0
    45-49    227853.0
    50-54    124695.0
    55-59     61578.0
    60-64     23720.0
    65-69      5608.0
    70+      108486.0
    <16          22.0
    Name: Female, dtype: float64



### 5. What is the distribution of the private sectors in the UAE for each Occupation sorted ascending?


```python
df['Occupation'].value_counts().sort_values()
```




    X-NA                                                      169
    6-Skilled agricultural, forestry and fishery workers     2857
    1-Managers                                               8287
    7-Craft and related trades workers                       8657
    8-Plant and machine operators, and assemblers            9373
    2-Professionals                                         10026
    3-Technicians and associate professionals               10337
    5-Service and sales workers                             10743
    9-Elementary occupations                                10844
    4-Clerical support workers                              11707
    Name: Occupation, dtype: int64



### 6. What is the average of female workers count in each Occupation type sorted ascending?


```python
df.groupby("Occupation")['Female'].mean().sort_values()
```




    Occupation
    X-NA                                                      0.035503
    6-Skilled agricultural, forestry and fishery workers      0.555128
    8-Plant and machine operators, and assemblers             3.699563
    7-Craft and related trades workers                        4.194294
    1-Managers                                               21.047665
    3-Technicians and associate professionals                27.594273
    9-Elementary occupations                                 30.610476
    2-Professionals                                          73.908737
    4-Clerical support workers                               74.157256
    5-Service and sales workers                             110.142418
    Name: Female, dtype: float64



### 7. What is the count of the Managers sector in each emirate sorted ascending?


```python
df[df['Occupation'] == '1-Managers']['Emirate'].value_counts().sort_values()
```




    Um Al Qaiwain     587
    Fujairah          683
    Ras Al Khaima     794
    Ajman             849
    Dubai            1359
    Sharjah          1397
    Abu Dhabi        2618
    Name: Emirate, dtype: int64



### 8. What are the most frequent Age Level in Abu Dhabi emirate?


```python
counts = df[df['Emirate'] == "Abu Dhabi"]['Age_Class'].value_counts().sort_index()
ax = counts.plot.bar(color='green', figsize=(8, 6))
ax.set_title('Distribution of workers in private sectors in Abu Dhabi emirate based on Age Class')
ax.set_xlabel('Age Class')
ax.set_ylabel('Number of Private sectors')
plt.show()

```


    
![png](images/output_52_0.png)
    


### 9.1 What is ratio of sectors with no female workers in each city?


```python
counts = df[df['Female'] == 0]['City'].value_counts()
ax = counts.plot.pie( figsize=(8, 10))
ax.set_title('Ratio of private sectors with no female workers in each city in the UAE')
plt.show()
```


    
![png](images/output_54_0.png)
    


#### 9.2 What is ratio of sectors with female workers only in each city?


```python
counts = df[df['Male'] == 0]['City'].value_counts()
ax = counts.plot.pie( figsize=(8, 10))
ax.set_title('Ratio of private sectors with female workers only in each city in the UAE')
plt.show()
```


    
![png](images/output_56_0.png)
    


### 10. What is the distribution of the private sectors based on the Activity type?


```python
counts = df['Activity'].value_counts()
ax = counts.plot.barh( figsize=(7, 5))
ax.set_title('Distribution of the private sectors based on the type of activity')
ax.set_xlabel('Activity type')
ax.set_ylabel('Sectors count')
plt.show()
```


    
![png](images/output_58_0.png)
    

